<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Membre;

class CniospController extends Controller
{
    public function About(){
        return view('pages.about');
    }
    
    public function Historique(){
        return view('pages.historique');
    }
    
    public function Activites(){
        return view('pages.activites');
    }
    
    public function Membres(){
        $membres= Membre::all();
        return view('pages.membre',[
            'membres'=>$membres,
        ]);
    }
    
    public function Personnel(){
        return view('pages.personnel');
    }
    
    public function Contact(){
        return view('pages.contact');
    }
}
