<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/about', 'CniospController@About')->name('apropos');
Route::get('/contact', 'CniospController@Contact')->name('contact');
Route::get('/historique', 'CniospController@Historique')->name('historique');
Route::get('/activites', 'CniospController@Activites')->name('activites');
Route::get('/membres', 'CniospController@Membres')->name('membre');
Route::get('/personnel', 'CniospController@Personnel')->name('personnel');
Route::get('/contact', 'CniospController@Contact')->name('contact');
