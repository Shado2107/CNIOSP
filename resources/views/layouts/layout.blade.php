@include('includes._header')

<body>
    <!--? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src=" {{ asset('utilisateurs/assets/img/logo/loder.png') }} " alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
 
 @include('includes._navbar')
 
   
   @yield('content')
   
   
 @include('includes._footer')