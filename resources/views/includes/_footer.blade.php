<footer>
    <!--? Footer Start-->
    <div class="footer-area footer-bg">
        <div class="container">
            <div class="footer-top footer-padding">
                <!-- footer Heading -->
                <div class="footer-heading">
                    <div class="row justify-content-between">
                        <div class="col-xl-6 col-lg-7 col-md-10">
                            <div class="footer-tittle2">
                                <h4>Stay Updated</h4>
                            </div>
                            <!-- Form -->
                            <div class="footer-form mb-50">
                                <div id="mc_embed_signup">
                                    <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative mail_part" novalidate="true">
                                        <input type="email" name="EMAIL" id="newsletter-form-email" placeholder=" Email Address " class="placeholder hide-on-focus" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address'">
                                        <div class="form-icon">
                                            <button type="submit" name="submit" id="newsletter-submit" class="email_icon newsletter-submit button-contactForm">
                                                Subscribe Now
                                            </button>
                                        </div>
                                        <div class="mt-10 info"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5">
                            <div class="footer-tittle2">
                                <h4>Let’s Get Social</h4>
                            </div>
                            <!-- Footer Social -->
                            <div class="footer-social">
                                <a href="https://www.facebook.com/sai4ull"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                {{-- <a  href="#"><i class="fab fa-google"></i></a> --}}
                                <a href="#"><i class="fab fa-instagram"></i></a>
                                {{-- <a href="#"><i class="fab fa-youtube"></i></a> --}}
                              </div>
                        </div>
                    </div>
                </div>
                <!-- Footer Menu -->
                <div class="row d-flex justify-content-between">
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>A propos</h4>
                                <ul>
                                    <li><a href="#">Historique</a></li>
                                    <li><a href="#">Objectifs</a></li>
                                    <li><a href="#">Activités</a></li>
                                    <li><a href="#">Membres</a></li>
                                    <li><a href="#">Personnel</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Pages</h4>
                                <ul>
                                    <li><a href="#">Acceuil</a></li>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Historique</a></li>
                                    <li><a href="#">Activités</a></li>
                                    <li><a href="#">A propos</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Liens utiles</h4>
                                <ul>
                                    <li><a href="#">Ministere de l'Enseignement</a></li>
                                    <li><a href="#">Getting Started</a></li>
                                    <li><a href="#">Online Application</a></li>
                                    <li><a href="#">Visa Information</a></li>
                                    <li><a href="#">Tuition Calculator</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Contact</h4>
                                <ul>
                                    <li><a href="#">CNIOSP 07BP :12343</a></li>
                                    <li><a href="#">cniosp@yahoo.fr</a></li>
                                    <li><a href="#">(+229) 22 21 30 47 / 22 21 60 43 </a></li>
                                    {{-- <li><a href="#">Register Activation Key</a></li>
                                    <li><a href="#">Site feedback</a></li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer Bottom -->
            <div class="footer-bottom">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-12">
                        <div class="footer-copy-right text-center">
                            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://creazionestudio.com" target="_blank">Creazione studio</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
</footer>
<!-- Scroll Up -->
<div id="back-top" >
    <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
</div>

<!-- JS here -->

<script src="{{URL::asset('utilisateurs/assets/js/vendor/modernizr-3.5.0.min.js')}} "></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="{{URL::asset('utilisateurs/assets/js/vendor/jquery-1.12.4.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/popper.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/bootstrap.min.js')}} "></script>
<!-- Jquery Mobile Menu -->
<script src="{{URL::asset('utilisateurs/assets/js/jquery.slicknav.min.js')}} "></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="{{URL::asset('utilisateurs/assets/js/owl.carousel.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/slick.min.js')}} "></script>
<!-- One Page, Animated-HeadLin -->
<script src="{{URL::asset('utilisateurs/assets/js/wow.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/animated.headline.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/jquery.magnific-popup.js')}} "></script>

<!-- Date Picker -->
<script src="{{URL::asset('utilisateurs/assets/js/gijgo.min.js')}} "></script>
<!-- Nice-select, sticky -->
<script src="{{URL::asset('utilisateurs/assets/js/jquery.nice-select.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/jquery.sticky.js')}} "></script>

<!-- counter , waypoint -->
<script src="{{URL::asset('utilisateurs/assets/js/jquery.counterup.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/waypoints.min.js')}} "></script>

<!-- contact js -->
<script src="{{URL::asset('utilisateurs/assets/js/contact.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/jquery.form.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/jquery.validate.min.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/mail-script.js')}} "></script>
<script src="{{URL::asset('utilisateurs/ssets/js/jquery.ajaxchimp.min.js')}} a"></script>

<!-- Jquery Plugins, main Jquery -->	
<script src="{{URL::asset('utilisateurs/assets/js/plugins.js')}} "></script>
<script src="{{URL::asset('utilisateurs/assets/js/main.js')}} "></script>

</body>
</html>